
'use strict';


const outputYou = document.querySelector('.output-you');
const outputBot = document.querySelector('.output-bot');
//invoke an instance of SpeechRecognition, the controller interface of the Web Speech API for voice recognition:

const SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
const recognition = new SpeechRecognition();
const socket = io();

recognition.lang = 'en-US';
recognition.interimResults = false;
recognition.maxAlternatives = 1;

//capture the DOM reference for the button UI, and listen for the click event to initiate speech recognition
document.querySelector('button').addEventListener('click', () => {
  recognition.start();
});

//Once speech recognition has started, use the result event to retrieve what was said as text
recognition.addEventListener('result', (e) => {
  let last = e.results.length - 1;
  let text = e.results[last][0].transcript;

outputYou.textContent = text;
  console.log('Confidence: ' + e.results[0][0].confidence);

socket.emit('chat message', text);
  // We will use the Socket.IO here later…
});

recognition.addEventListener('speechend', () => {
  recognition.stop();
});

recognition.addEventListener('error', (e) => {
  outputBot.textContent = 'Error: ' + e.error;
});

function synthVoice(text) {
  const synth = window.speechSynthesis;
  const utterance = new SpeechSynthesisUtterance();
  utterance.text = text;
  synth.speak(utterance);
}

socket.on('bot reply', function(replyText) {
  synthVoice(replyText);

  if(replyText == '') replyText = '(No answer...)';
  outputBot.textContent = replyText;
});

